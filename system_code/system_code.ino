
// includes
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include "string.h"
#include <Stepper.h>   // Include the Arduino Stepper Library

// defines
#define SERVOMIN  150 // This is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  600 // This is the 'maximum' pulse length count (out of 4096)
#define USMIN  600 // This is the rounded 'minimum' microsecond length based on the minimum pulse of 150
#define USMAX  2400 // This is the rounded 'maximum' microsecond length based on the maximum pulse of 600
#define SERVO_FREQ 50 // Analog servos run at ~50 Hz updates
#define OSC_FREQ 27000000 // The int.osc. is closer to 27MHz  
#define STEPS_PER_REV 200



// object declarations
Adafruit_PWMServoDriver servo_driver = Adafruit_PWMServoDriver();
Stepper left_x_stepper(STEPS_PER_REV, 52, 50, 48, 46);       // Create Instance of Stepper library
Stepper left_y_stepper(STEPS_PER_REV, 53, 51, 49, 47); 


// global variables
String input_string;
String query_string;
String actuator_string;
String orientation_string;
String servo_string;
int steps_to_move;
int servo_pin;
int desired_pos;






void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
Serial.println("Welcome to the Brazing Bunch Automatic Brazing Machine.");
Serial.println("This UI is still in early development, and is currently supporting a couple simple commands for demonstration purposes.");
Serial.println("Enter the command 'help' to see the full list of commands.");


// setup devices
  setup_servo_driver();
  setup_actuator_motors();



}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print(">> ");
    input_string = getInput();
    Serial.println(input_string);
    if(input_string == "help"){
        Serial.println("Supported commands: \n\t help \n\t servo \n\t actuator \n\t query");
    }
    else if(input_string == "servo"){
        get_servo_input();
        delay(3000);
    }
    else if(input_string == "actuator"){
        get_actuator_input();
        delay(3000);
    }
    else if(input_string == "query"){
        get_query_input();
    }
    else{
        Serial.println();
        Serial.print(input_string);
        Serial.println(" is not a supported command, please try 'help' command to see full list of supported commands");
    }
}




/********UI Input Functions********/

    String getInput(){
        String temp_string;
        while(Serial.available() == 0){}
        temp_string = Serial.readString();
        temp_string.trim();
        return temp_string;
    }

/********UI Input Functions********/


/**********Servo Functions*********/

      void get_servo_input(){
        Serial.println("Please enter which servo you would like to adjust, 1-8");
        servo_string = getInput();
        Serial.print("Servo Selected: ");
        Serial.println(servo_string.toInt());
        if(isValidServo(servo_string.toInt())){
          servo_pin = servo_string.toInt() - 1;
          Serial.print("Position Desired: ");
          desired_pos = getInput().toInt();
          Serial.println(desired_pos);
          if(isValidPos(desired_pos)){
             Serial.println("Moving... please wait until finished");
             servoToAngle(desired_pos,servo_pin);
          }
          else{
            Serial.println("Invalid Position");
          }
        }
        else{
          Serial.println("Invalid Servo");
        }   
      }
      
      void setup_servo_driver(){
        servo_driver.begin();
        servo_driver.setOscillatorFrequency(OSC_FREQ); 
        servo_driver.setPWMFreq(SERVO_FREQ); 
      }
      
      void servoToAngle(uint8_t angle, uint8_t pin){
          int pulselength = map(angle, 0, 180, SERVOMIN, SERVOMAX);
          servo_driver.setPWM(pin, 0, pulselength);
      }
      
      bool isValidPos(int position){
        if(position >=60 && position <= 120){
          return true;
        }
        return false;
      }
      
      // update this for 16 servos later
      bool isValidServo(int servo_pin){
        if(servo_pin >=1 && servo_pin <= 8){
          return true;
        }
        return false;
      }

      
/**********Servo Functions*********/


/*********Actuator Functions********/

      void get_actuator_input(){
        
        Serial.println("Please enter which actuator you would like to adjust, 'left' or 'right':");
        actuator_string = getInput();
        Serial.print("Actuator Selected: ");
        Serial.println(actuator_string);

        Serial.println("Please enter which orientation you would like to adjust, 'x' or 'y': ");
        orientation_string = getInput();
        Serial.print("Orientation Selected: ");
        Serial.println(orientation_string);
        Serial.println("Please enter how many steps you would like it to move: ");
       
        Serial.print("Steps Desired: ");
        steps_to_move = getInput().toInt();
        Serial.println(steps_to_move);
        Serial.println("Moving... please wait until finished");
        move_individual_motor(orientation_string,actuator_string,steps_to_move);
      }

      void setup_actuator_motors(){
        left_x_stepper.setSpeed(60);                             
        left_y_stepper.setSpeed(60);
        Serial.println("Zero-ing all motors");
        // zero_motor(left,x);
        // zero_motor(left,y);
        // zero_motor(right,x);
        // zero_motor(right,y);
      }

      void move_individual_motor(String orient_string, String actuator_string, int steps){

        if(!strcmp("left", actuator_string.c_str()) && (!strcmp("x",orient_string.c_str()))){
          left_x_stepper.step(steps);
          Serial.println("Moving Moter left x");
        }
        if(!strcmp("left", actuator_string.c_str()) && (!strcmp("y",orient_string.c_str()))){
          left_y_stepper.step(steps);
          Serial.println("Moving Moter left y");
        }
/*      if(!strcmp("right", orient_string) && (!strcmp("x",orient_string))){
          left_x_stepper.step(steps);
        }
        if(!strcmp("right", orient_string) && (!strcmp("y",orient_string))){
          left_x_stepper.step(steps);
        }
*/

      }

/*********Actuator Functions********/


/**********Query Functions*********/
        
      void get_query_input(){  
        Serial.println("You can query the following measurements: temp, etc.");
 
        Serial.println("Please enter what device you would like to query: ");
        query_string = getInput();
        Serial.println(query_string);
        Serial.print("Querying: ");
        Serial.println(query_string);
        Serial.println("Temperature: 40 degrees c");
      }

/**********Query Functions*********/


/**********MFC Functions***********/





/**********MFC Functions***********/



/*********Node_Red Functions*********/





/*********Node_Red Functions*********/
