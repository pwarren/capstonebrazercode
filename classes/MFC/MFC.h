/*
  MFC.h - Library for controlling MFC
  Created by Patrick E. Warren, February 21, 2020.
  Released into the public domain.
*/
#ifndef MFC_h
#define MFC_h

#include "Arduino.h"

class MFC
{
  public:
    Morse(int pin);
    void dot();
    void dash();
  private:
    int _signal_output_pin;
    int _command_input_pin;
    int _valve_override_pin;
};

#endif



// what is needed to initialize a MFC

// command input pin, signal output pin, valve override pin
