/*
  MFC.h - Library for controlling MFC
  Created by Patrick E. Warren, February 21, 2020.
  Released into the public domain.
*/
class MFC {
  
    public:
      MFC(int,int,);
      

  
}

// what is needed to initialize a MFC

// command input pin, signal output pin, valve override pin





/*
  Morse.cpp - Library for flashing Morse code.
  Created by David A. Mellis, November 2, 2007.
  Released into the public domain.
*/

#include "Arduino.h"
#include "Morse.h"

Morse::Morse(int pin)
{
  pinMode(pin, OUTPUT);
  _pin = pin;
}

void Morse::dot()
{
  digitalWrite(_pin, HIGH);
  delay(250);
  digitalWrite(_pin, LOW);
  delay(250);  
}

void Morse::dash()
{
  digitalWrite(_pin, HIGH);
  delay(1000);
  digitalWrite(_pin, LOW);
  delay(250);
}