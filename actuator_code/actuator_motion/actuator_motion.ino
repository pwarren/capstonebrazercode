#include <stepper_motor.h>

#include <Stepper.h>                                            // Include the Arduino Stepper Library

// Variables
const int steps_per_revolution = 200;                           // Number of steps per output rotation
const int switch_pin = 2;                                       // Normally open pin of micro switch
int stop_all = 0;                                                  // State of micro switch
Stepper vert_stepper(steps_per_revolution, 8, 9, 10, 11);       // Create Instance of Stepper library
Stepper horiz_stepper(steps_per_revolution, 4, 5, 6, 7);        // Create Instance of Stepper library

void setup() {
  vert_stepper.setSpeed(60);                                    // Set the speed at 60 rpm
  horiz_stepper.setSpeed(60);                                   // Set the speed at 60 rpm
  pinMode(switch_pin, INPUT);                                   // Sets the switch pin as an input
  
  Serial.begin(9600);                                           // Initialize the serial port
}


void loop() {

  // Step one revolution horizontally and vertically and then in reverse
  vert_stepper.step(steps_per_revolution);
  horiz_stepper.step(steps_per_revolution);
  vert_stepper.step(-steps_per_revolution);
  horiz_stepper.step(-steps_per_revolution);
  delay(500);

/*
  // Move horizontally until stop button is activated
  if (stop_all == 1) {
    horiz_stepper.step(steps_per_revolution/8);
  }
  stop_all = digitalRead(switch_pin);
  Serial.println(stop_all);
*/ 
}
