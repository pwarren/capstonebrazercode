#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

#define SERVOMIN  150 // This is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  600 // This is the 'maximum' pulse length count (out of 4096)
#define USMIN  600 // This is the rounded 'minimum' microsecond length based on the minimum pulse of 150
#define USMAX  2400 // This is the rounded 'maximum' microsecond length based on the maximum pulse of 600
#define SERVO_FREQ 50 // Analog servos run at ~50 Hz updates



void setup() {
  Serial.begin(9600);
  Serial.println("8 channel Servo test!");

  pwm.begin();

  pwm.setOscillatorFrequency(27000000);  // The int.osc. is closer to 27MHz  
  pwm.setPWMFreq(SERVO_FREQ);  // Analog servos run at ~50 Hz updates
  delay(1000);
}


String getInput(){
    String temp_string;
    while(Serial.available() == 0){}
    temp_string = Serial.readString();
    temp_string.trim();
    return temp_string;
}

void servoToAngle(uint8_t angle, uint8_t pin){
    int pulselength = map(angle, 0, 180, SERVOMIN, SERVOMAX);
    pwm.setPWM(pin, 0, pulselength);
}

bool isValidPos(int position){
  if(position >=0 && position <= 180){
    return true;
  }
  return false;
}

// update this for 16 servos later
bool isValidServo(int servo_pin){
  if(servo_pin >=1 && servo_pin <= 8){
    return true;
  }
  return false;
}


String input_string;
String servo_string;

void loop() {
  int servo_pin;
  int desired_pos;
  //int pulselength;

  Serial.print(">> ");
  input_string = getInput();
  Serial.println(input_string);
  if(input_string == "servo"){
        Serial.println("Please enter which servo you would like to adjust, 1-8");
        servo_string = getInput();
        Serial.print("Servo Selected: ");
        Serial.println(servo_string.toInt());
        if(isValidServo(servo_string.toInt())){
          servo_pin = servo_string.toInt() - 1;
          Serial.print("Position Desired: ");
          desired_pos = getInput().toInt();
          Serial.println(desired_pos);
          if(isValidPos(desired_pos)){
             Serial.println("Moving... please wait until finished");
             servoToAngle(desired_pos,servo_pin);
          }
          else{
            Serial.println("Invalid Position");
          }
        }
        else{
          Serial.println("Invalid Servo");
        }
    }
    else{
        Serial.println();
        Serial.print(input_string);
        Serial.println(" is not a supported command, please try 'help' command to see full list of supported commands");
    }

    delay(3000);
}
